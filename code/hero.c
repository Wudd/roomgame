#include"hero.h"
#include"things.h"
#include"roomui.h"
#include"heroui.h"
#include<stdio.h>
#include<stdlib.h>

//坦克初始化 
hero_you *initTANK(){
	hero_you *hy ;
	hy = (hero_you *)malloc(sizeof(hero_you)) ;
	
	hy->job = TANK ;
	
	hy->hp = 250 ;
	hy->hp_max = 250 ;
	
	hy->mp = 50 ;
	hy->mp_max = 50 ;
	
	hy->strength = 20 ;
	hy->energy = 5 ;
	
	hy->type = HEALTH ;
	hy->level = 1 ;
	hy->gold = 0 ;
	
	hy->face =prTANK_face ;
	hy->arm = prTANK_hands ;
	
	int i ;
	for(i = 0 ; i < MAX_EQUIP ; i ++){
		hy->equips[i] = getEmtpy() ;
	}
	
	hy->equip_up_hp = 0 ; hy->equip_up_mp = 0 ; 
	hy->equip_up_strength = 0 ; hy->equip_up_energy = 0 ;
	
	hy->hp_end = hy->hp_max + hy->equip_up_hp ;
	hy->mp_end = hy->mp_max + hy->equip_up_mp ;
	hy->strength_end = hy->strength + hy->equip_up_strength ;
	hy->energy_end = hy->energy + hy->equip_up_energy ;
	
	hy->can_equiped_num = 3 ;
	
	return hy ;
}
//剑士初始化 
hero_you *initSWORDMAN(){
	hero_you *hy ;
	hy = (hero_you *)malloc(sizeof(hero_you)) ;
	
	hy->job = SWORDMAN ;
	
	hy->hp = 100 ;
	hy->hp_max = 100 ;
	
	hy->mp = 75 ;
	hy->mp_max = 75 ;
	
	hy->strength = 50 ;
	hy->energy = 7 ;
	
	hy->type = HEALTH ;	
	hy->level = 1 ;
	hy->gold = 0 ;
	
	hy->face = prSWORDMAN_face ;
	hy->arm = prSWORDMAN_hands ;
	
	int i ;
	for(i = 0 ; i < MAX_EQUIP ; i ++){
		hy->equips[i] = getEmtpy() ;
	}
	
	hy->equip_up_hp = 0 ; hy->equip_up_mp = 0 ; 
	hy->equip_up_strength = 0 ; hy->equip_up_energy = 0 ;
	
	hy->hp_end = hy->hp_max + hy->equip_up_hp ;
	hy->mp_end = hy->mp_max + hy->equip_up_mp ;
	hy->strength_end = hy->strength + hy->equip_up_strength ;
	hy->energy_end = hy->energy + hy->equip_up_energy ;
	
	hy->can_equiped_num = 3 ;
	
	return hy ;
}
//普通人初始化 
hero_you *initMAN(){
	hero_you *hy ;
	hy = (hero_you *)malloc(sizeof(hero_you)) ;
	
	hy->job = MAN ;
	
	hy->hp = 100 ;
	hy->hp_max = 100 ;
	
	hy->mp = 0 ;
	hy->mp_max = 0 ;
	
	hy->strength = 30 ;
	hy->energy = 5 ;
	
	hy->type = HEALTH ;	
	hy->level = 1 ;
	hy->gold = 0 ;
	
	hy->face = prMAN_face ;
	hy->arm = prMAN_hands ;
	
	int i ;
	for(i = 0 ; i < MAX_EQUIP ; i ++){
		hy->equips[i] = getEmtpy() ;
	}
	
	hy->equip_up_hp = 0 ; hy->equip_up_mp = 0 ; 
	hy->equip_up_strength = 0 ; hy->equip_up_energy = 0 ;
	
	hy->hp_end = hy->hp_max + hy->equip_up_hp ;
	hy->mp_end = hy->mp_max + hy->equip_up_mp ;
	hy->strength_end = hy->strength + hy->equip_up_strength ;
	hy->energy_end = hy->energy + hy->equip_up_energy ;
	
	hy->can_equiped_num = 3 ;
	
	return hy ;
}
//法师初始化 
hero_you *initENCHANTER(){
	hero_you *hy ;
	hy = (hero_you *)malloc(sizeof(hero_you)) ;
	
	hy->job = ENCHANTER ;
	
	hy->hp = 75 ;
	hy->hp_max = 75 ;
	
	hy->mp = 200 ;
	hy->mp_max = 200 ;
	
	hy->strength = 15 ;
	hy->energy = 50 ;
	
	hy->type = HEALTH ;	
	hy->level = 1 ;
	hy->gold = 0 ;
	
	hy->face = prENCHANTER_face ;
	hy->arm = prENCHANTER_hands ;
	
	int i ;
	for(i = 0 ; i < MAX_EQUIP ; i ++){
		hy->equips[i] = getEmtpy() ;
	}
	
	hy->equip_up_hp = 0 ; hy->equip_up_mp = 0 ; 
	hy->equip_up_strength = 0 ; hy->equip_up_energy = 0 ;
	
	hy->hp_end = hy->hp_max + hy->equip_up_hp ;
	hy->mp_end = hy->mp_max + hy->equip_up_mp ;
	hy->strength_end = hy->strength + hy->equip_up_strength ;
	hy->energy_end = hy->energy + hy->equip_up_energy ;
	
	hy->can_equiped_num = 3 ;
	
	return hy ;
}

//获得已经装备上的装备的属性加成 
int getHeroEquip_up_hp(hero_you * hy){
	hy->equip_up_hp = 0 ;
	int i ;
	for(i = 0 ; i < MAX_EQUIP ; i ++){
		if(hy->equips[i]->canuse == 1){
			hy->equip_up_hp += hy->equips[i]->hpMax_up ;
		}
	}
	return hy->equip_up_hp ;
}

int getHeroEquip_up_mp(hero_you * hy){
	hy->equip_up_mp = 0 ;
	int i ;
	for(i = 0 ; i < MAX_EQUIP ; i ++){
		if(hy->equips[i]->canuse == 1){
			hy->equip_up_mp += hy->equips[i]->mpMax_up ;
		}
	}
	return hy->equip_up_mp ;	
}

int getHeroEquip_up_strength(hero_you * hy){
	hy->equip_up_strength = 0 ;
	int i ;
	for(i = 0 ; i < MAX_EQUIP ; i ++){
		if(hy->equips[i]->canuse == 1){
			hy->equip_up_strength += hy->equips[i]->strength_up ;
		}
	}	
	return hy->equip_up_strength ;
}

int getHeroEquip_up_energy(hero_you * hy){
	hy->equip_up_energy = 0 ;
	int i ;
	for(i = 0 ; i < MAX_EQUIP ; i ++){
		if(hy->equips[i]->canuse == 1){
			hy->equip_up_energy += hy->equips[i]->energy_up ;
		}
	}
	return hy->equip_up_energy ;	
}
//判断背包是否已满 
int equipFull(hero_you * hy){
	int ret = 1 ;
	
	int i ; 
	for(i = 0 ; i < MAX_EQUIP ; i ++){
		if(hy->equips[i]->type == Emtpy){
			ret = 0 ;
			break ;
		}
	}
	
	return ret ;
}
//判断背包是否已空 
int equipEmpty(hero_you * hy){
	int ret = 1 ;
	
	int i ;
	for(i = 0 ; i < MAX_EQUIP ; i ++){
		if(hy->equips[i]->type != Emtpy){
			ret = 0 ;
			break ;
		}
	}
	
	return ret ;
}

//背包加入装备 
int equipUpdate(hero_you * hy , equip * ep){
	int ret = 1 ;
	if(equipFull(hy)){
		ret = 0 ;
		goto OUT ;	
	}
	
	int i ;
	for(i = 0 ; i < MAX_EQUIP ; i ++){
		if(hy->equips[i]->type == Emtpy){
			hy->equips[i] = ep ;
			break ;
		}
	}
	
OUT :
	return ret ;
}
//背包减去装备 
int equipDecdate(hero_you * hy , equip * ep){
	int ret = 1 ;
	if(equipEmpty(hy)){
		ret = 0 ;
		goto OUT ;
	}	
	
	equip * emptyEp ;
	emptyEp = getEmtpy() ;
	
	int i ;
	for(i = 0 ; i < MAX_EQUIP ; i ++){
		if(hy->equips[i]->type == ep->type){
			hy->equips[i] = emptyEp ; 
		}
	}
	
OUT :
	return ret ;
}

void prAboutHero(hero_you *hy){
	printf("\
---------------------------------------\n") ;
	printf("\
血量:%d/%d()               魔法值:%d/%d()\n\
力量:%d()        精神力:%d()      等级:%d\n\
金币:%d	  		    你	     			 \n\
" , hy->hp , hy->hp_max , hy->mp , hy->mp_max ,\
 hy->strength , hy->energy , hy->level , hy->gold) ;
	printf("\
---------------------------------------\n") ;
}

hero_you *creatHERO(){
	printf("选择你的职业:\n") ;
	hero_you * u ;
	u = (hero_you *)malloc(sizeof(hero_you)) ;
	printf("1.坦克\n") ;
	prEmtop() ;
	prTANK_face() ;
	prEmtop() ;
	printf("2.剑士\n") ;
	prEmtop() ;
	prSWORDMAN_face() ;
	prEmtop() ;
	printf("3.普通人\n") ;
	prEmtop() ;
	prMAN_face() ;
	prEmtop() ;
	printf("4.法师\n") ;
	prEmtop() ;
	prENCHANTER_face() ;
	prEmtop() ;
	
	int input ;
	printf("选择:\n") ;
	while(1){
		input = inputAction() ;
		input -= '0' ;
		if(input == 1 || input == 2 || input == 3 || input == 4){
			break ;
		}else{
			printf("输入错误!\n") ;
		}
	}
		
	switch(input){
		case 1 :
			u = initTANK() ;
			break ;
		case 2 :
			u = initSWORDMAN() ;
			break ;
		case 3 :
			u = initMAN() ;
			break ;
		case 4 :
			u = initENCHANTER() ;
			break ;
	}
	system("CLS") ;
	printf("人物创建成功！\n") ;
	printf("你的人物是：\n") ;
	u->face() ;
	prAboutHero(u) ;
	
	system("pause") ;
	
	return u ;
}
