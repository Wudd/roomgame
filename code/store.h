#ifndef _STORE_
#define _STORE_

#include"hero.h"
#include"heroui.h"
#include<stdio.h>
#include<stdlib.h>

#define ATTRIBUTE_NUMS 10

extern hero_you * getStore() ;
extern int storeHero(hero_you * hy) ;

#endif
