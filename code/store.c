#include"store.h"

hero_you * getStore(){
	hero_you * hy ;
	hy = (hero_you *)malloc(sizeof(hero_you)) ;
	FILE * pf ;
	pf = fopen("data.txt" , "r+") ;
	int attribute[ATTRIBUTE_NUMS] ;
	memset(attribute , 0 , sizeof(attribute)) ;
	fread(attribute , sizeof(attribute) , 1 , pf) ;
	int i = 0 ;
	hy->job = attribute[i] ;
	i ++ ; hy->hp       = attribute[i] ;
	i ++ ; hy->hp_max   = attribute[i] ;
	i ++ ; hy->mp       = attribute[i] ;
	i ++ ; hy->mp_max   = attribute[i] ;
	i ++ ; hy->strength = attribute[i] ;
	i ++ ; hy->energy   = attribute[i] ;
	i ++ ; hy->level    = attribute[i] ;
	i ++ ; hy->type     = attribute[i] ;
	i ++ ; hy->gold     = attribute[i] ;
	
	switch(hy->job){
		case TANK :
			hy->face = prTANK_face ;
			hy->arm = prTANK_hands ;
			break ;
		case SWORDMAN :
			hy->face = prSWORDMAN_face ;
			hy->arm = prSWORDMAN_hands ;
			break ;
		case MAN :
			hy->face = prMAN_face ;
			hy->arm = prMAN_hands ;
			break ;
		case ENCHANTER :
			hy->face = prENCHANTER_face ;
			hy->arm = prENCHANTER_hands ;
			break ;
	}
	
	return hy ;
}

int storeHero(hero_you * hy){
	int ret = 0 ;
	FILE * pf ;
	
	pf = fopen("data.txt" , "w+") ;
	
	int attribute[ATTRIBUTE_NUMS] ;
	memset(attribute , 0 , sizeof(attribute)) ;
	int i = 0 ;
	attribute[i] = hy->job ;
	i ++ ; attribute[i] = hy->hp ;
	i ++ ; attribute[i] = hy->hp_max ;
	i ++ ; attribute[i] = hy->mp ;
	i ++ ; attribute[i] = hy->mp_max ;
	i ++ ; attribute[i] = hy->strength ;
	i ++ ; attribute[i] = hy->energy ;
	i ++ ; attribute[i] = hy->level ;
	i ++ ; attribute[i] = hy->type ;
	i ++ ; attribute[i] = hy->gold ;
	
	fwrite(attribute , sizeof(attribute) , 1 , pf) ;
	fclose(pf) ;
	
OUT :	
	return ret ;
}
