#ifndef _STRONGER_H
#define _STRONGER_H
#include"hero.h"
#include"main.h"


static int upHp = 20 ;
static int upMp = 50 ;
static int upStrength = 25 ;
static int upEnergy = 15 ;


static int consumeUpHp = 30 ;
static int consumeUpMp = 20 ;
static int consumeUpStrength = 50 ;
static int consumeUpEnergy = 60 ;


extern void consumeMoney(hero_you *hy) ;

#endif
