#ifndef _HEROUI_H
#define _HEROUI_H

extern void prTANK_face() ;
extern void prSWORDMAN_face() ;
extern void prMAN_face() ;
extern void prENCHANTER_face() ;

extern void prTANK_hands() ;
extern void prSWORDMAN_hands() ;
extern void prMAN_hands() ;
extern void prENCHANTER_hands() ; 

#endif
