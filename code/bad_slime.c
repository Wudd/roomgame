#include"bad_slime.h"

void prBad_slime(badboy *sl ){
	printf("\
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n") ;
	printf("\
 血量:%d/%d               魔法值:%d/%d \n\
 力量:%d        精神力:%d      等级:%d    \n\
 金币:%d	   史莱姆 		       \n\
" , sl->hp , sl->hp_max , sl->mp , sl->mp_max , sl->strength , sl->energy , sl->level , sl->diegold) ;
	printf("\
vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv\n") ;	
}

void prBad_slime2(badboy *sl ){
	printf("\
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n") ;
	printf("\
 血量:%d/%d               魔法值:%d/%d \n\
 力量:%d        精神力:%d      等级:%d    \n\
 金币:%d	   中史莱姆 	       \n\
" , sl->hp , sl->hp_max , sl->mp , sl->mp_max , sl->strength , sl->energy , sl->level , sl->diegold) ;
	printf("\
vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv\n") ;	
}

void prBad_slime3(badboy *sl ){
	printf("\
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n") ;
	printf("\
 血量:%d/%d               魔法值:%d/%d \n\
 力量:%d        精神力:%d      等级:%d    \n\
 金币:%d	   小史莱姆		       \n\
" , sl->hp , sl->hp_max , sl->mp , sl->mp_max , sl->strength , sl->energy , sl->level , sl->diegold) ;
	printf("\
vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv\n") ;	
}

void prSlimeHurt(){
	prDrtop() ;
	printf("\
  生气了！       \n\
   __________ =  \n\
  |###  #   | =  \n\
  |#\\  ## / |=   \n\
  |#   @ @   |   \n\
  |# ________ |  \n\
 |##|----_---| | \n\
 |# |___|||__| | \n\
 |#______^^____| \n\
                 \n") ;	
}

void prSlimeHurt1(){
	prDrtop() ;
	printf("\
   打出包来了！  \n\
     # #         \n\
         #       \n\
       # #  ()   \n\
    __#__##( )   \n\
   | \\    / |    \n\
   | @@  @@ |    \n\
  |  $$$ $$$ |   \n\
   |____^_____|  \n\
                 \n") ;	
}

void prSlimeHurt2(){
	prDrtop() ;
	printf("\
                 \n\
         呜~     \n\
         痛！    \n\
         痛！    \n\
                 \n\
       ()        \n\
      (#)        \n\
     (##)__      \n\
    /~_|__~\\     \n\
                 \n") ;		
}

badboy * slimeInit(){
	badboy *s1 ;
	s1 = (badboy *)malloc(sizeof(struct BADMAN)) ;
	
	s1->name = "大史莱姆" ;
	s1->badnum = SLIME ; 
	s1->hp = 300 ;
	s1->hp_max = 300 ;
	s1->mp = 5 ;
	s1->mp_max = 5 ;
	
	s1->strength = 20 ;
	s1->energy = 20 ;
	
	s1->diegold = 75 ;
	s1->level = 1 ;
	
	s1->type = HEALTH ;
	s1->face = prBmnroom4 ;
	s1->about = prBad_slime ;
	s1->hurt = prSlimeHurt ;
	
	return s1 ;
}

badboy * slime2Init(){
	badboy *s2 ;
	s2 = (badboy *)malloc(sizeof(struct BADMAN)) ;
	
	s2->name = "中史莱姆" ;
	s2->badnum = SLIME2 ; 
	s2->hp = 150 ;
	s2->hp_max = 150 ;
	s2->mp = 5 ;
	s2->mp_max = 5 ;
	
	s2->strength = 10 ;
	s2->energy = 10 ;
	
	s2->diegold = 30 ;
	s2->level = 1 ;
	
	s2->type = HEALTH ;
	s2->face = prBmnroom4_1 ;
	s2->about = prBad_slime2 ;
	s2->hurt = prSlimeHurt1 ;
	
	return s2 ;
}

badboy * slime3Init(){
	badboy *s3 ;
	s3 = (badboy *)malloc(sizeof(struct BADMAN)) ;
	
	s3->name = "迷你史莱姆" ;
	s3->badnum = SLIME3 ; 
	s3->hp = 30 ;
	s3->hp_max = 30 ;
	s3->mp = 5 ;
	s3->mp_max = 5 ;
	
	s3->strength = 3 ;
	s3->energy = 3 ;
	
	s3->diegold = 5 ;
	s3->level = 1 ;
	
	s3->type = HEALTH ;
	s3->face = prBmnroom4_2 ;
	s3->about = prBad_slime3 ;
	s3->hurt = prSlimeHurt2 ;
	
	return s3 ;
}


