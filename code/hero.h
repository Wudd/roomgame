#ifndef _HERO_H
#define _HERO_H
#include"things.h"
#include"main.h"
typedef struct XMAN{
	int hp ;//血量
	int hp_max ;//血量上限 
	int mp ;//魔法值
	int mp_max ;//魔法值上限
	
	int strength ;//力量(影响攻击力)
	int energy ;//精神（影响魔法效果）
	
	int level ;//等级(升级可以增加 hp_max , mp_max , strength , energy) 
	int job ;//职业 (不同职业不同属性) 
	int type ; //人物状态 
	
	int gold ;//金币数 
	
	void (*face)(void) ;//脸部和武器的打印指针 
	void (*arm)(void) ;
	
	equip * equips[MAX_EQUIP] ;
	int equip_up_hp ;
	int equip_up_mp ;
	int equip_up_strength ;
	int equip_up_energy ;
	
	int can_equiped_num ;
	
	int hp_end ;
	int mp_end ;
	int strength_end ;
	int energy_end ;
	
	
}hero_you;

enum youjob{
	TANK = 20 ,//受到伤害会反弹伤害(受精神影响) 
	SWORDMAN ,//连续攻击两次 (与力量相关)
	MAN ,//没有魔法，一次普通攻击一次
	ENCHANTER ,//法师，很多花里胡哨的法术 
};

enum youtype{
	HEALTH = 30 ,
	DIE ,
};

extern hero_you *initTANK() ;
extern hero_you *initSWORDMAN() ;
extern hero_you *initMAN() ;
extern hero_you *initENCHANTER() ;

extern hero_you *creatHERO() ;
extern void prAboutHero(hero_you *hy) ;

#endif
