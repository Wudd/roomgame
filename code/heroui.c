#include<stdio.h>
#include"heroui.h"
#include"roomui.h"

//print face
void prTANK_face(){
	printf("\
|  ____--===--_ |\n\
|  |IIIIIIIIIII||\n\
|  |I|*  |I|*|I||\n\
|  |I|///|I|/|I||\n\
|   |    qp   | |\n\
|   |  -----  | |\n") ;
}

void prSWORDMAN_face(){
	printf("\
|               |\n\
|     _-=0=-_   |\n\
|    {~~~~ ~~}  |\n\
|   (| *   * |) |\n\
|    |   qp  |  |\n\
|    |   --  |  |\n") ;	
}

void prMAN_face(){
	printf("\
|      0000     |\n\
|     000000    |\n\
|     000000    |\n\
|      0000     |\n\
|   0000000000  |\n\
|  000000000000 |\n") ;	
}

void prENCHANTER_face(){
	printf("\
|   _-=====--_  |\n\
|  |          | |\n\
|  | |||||||| | |\n\
|  | |*    *| | |\n\
|  | |  qp  | | |\n\
|   ||  ^   ||  |\n") ;	
}
//print hands
void prTANK_hands(){
	printf("\
|   /\\__ __     |\n\
|  |    =| |    |\n\
|  |       |    |\n\
|  |  __=|_|    |\n\
|   \\/   | |    |\n") ;
}

void prSWORDMAN_hands(){
	printf("\
|     / |\\      |\n\
|    /  | \\     |\n\
|   /   |  \\    |\n\
|   |   |  |    |\n\
|   |   |  |    |\n") ;	
}

void prMAN_hands(){
	printf("\
|     \\|/       |\n\
|    --0--      |\n\
|     /|\\       |\n\
|      |        |\n\
|   (|)|(|)     |\n") ;	
}

void prENCHANTER_hands(){
	printf("\
|      00       |\n\
|     0000      |\n\
|     0*00      |\n\
|     000       |\n\
|     00        |\n") ;	
}
