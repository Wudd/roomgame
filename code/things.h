#ifndef _THINGS_H
#define _THINGS_H
#include<stdio.h>
#include<stdlib.h>
#define MAX_EQUIP 10



typedef struct EQUIP{
	int type ;
	int canuse ;
	int hpMax_up ;
	int mpMax_up ;
	int strength_up ;
	int energy_up ;
	
}equip;

typedef enum{
	Emtpy = 60 ,
	TANKarm , 
	SWORDMANarm ,
	MANarm ,
	ENCHANTERarm ,
	GUN ,
	DOG ,
	GetHp ,
	GetMp ,
	LORICAE
}things_type;

extern equip * getEmtpy() ;
extern equip * getTANKSuperArm() ;
extern equip * getSWORDMANSuperArm() ;
extern equip * getMANSuperArm() ;
extern equip * getENCHANTERSuperArm() ;
extern equip * getGUN() ;
extern equip * getDOG() ;
extern equip * getGetHp() ;
extern equip * getGetMp() ;
extern equip * getLoricae() ;
extern void prEmtpyArm() ;
extern void prTANKSuperArm() ;
extern void prSWORDMANSuperArm() ;
extern void prMANSuperArm() ;
extern void prENCHANTERSuperArm() ;
extern void prGUN() ;
extern void prDOG() ;
extern void prGetHp() ;
extern void prGetMp() ;
extern void prLoricae() ;


#endif
