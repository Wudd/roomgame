#ifndef _BAD_ALL_H
#define _BAD_ALL_H

#if 0

#include<string.h>
#include"bad_daimon.h"
#include"bad_peiqi.h"
#include"bad_slime.h"
#include"bad_snake.h"

#endif

typedef struct BADMAN{
	char *name ;
	
	int hp ;
	int hp_max ;
	int mp ;
	int mp_max ;
	
	int strength ;
	int energy ;
	
	int level ;
	int type ;
	
	int diegold ;
	int badnum ;
	
	void (*face)(void) ;
	void (*about)(struct BADMAN *bb) ;
	void (*hurt)(void) ;
	
}BadBoy;

typedef BadBoy badboy ;

enum BadNum{
	DAIMON = 50 ,
	SNAKE ,
	PEIQI ,
	SLIME ,
	SLIME2 ,
	SLIME3 ,
};

enum TYPE{
	BAD_HEALTH = 40 ,
	BAD_DIE ,
};



#endif
