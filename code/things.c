#include"things.h"

void prEmtpyArm(){
	printf("\
|               |\n\
|               |\n\
|               |\n\
|               |\n\
|               |\n") ;		
}

void prTANKSuperArm(){
	printf("\
|   /\\__ __     |\n\
|-_|*   =| |    |\n\
|--|*      |    |\n\
|_-|* __=|_|    |\n\
|   \\/   | |    |\n") ;	
}

void prSWORDMANSuperArm(){
	printf("\
|     /*|\\      |\n\
|    / *|*\\     |\n\
|   /  *|* \\    |\n\
|   |  *|* |    |\n\
|***|  *|* |****|\n") ;		
}

void prMANSuperArm(){
	printf("\
| *   \\|/   **  |\n\
|* * --0--    **|\n\
|*  * /|\\   * * |\n\
| * *  |    *  *|\n\
|   (|)|(|)     |\n") ;		
}

void prENCHANTERSuperArm(){
	printf("\
| * *  00  * *  |\n\
|* *  0000  * * |\n\
|* *  0*00  * * |\n\
| * * 000  * *  |\n\
|     00        |\n") ;		
}

void prGUN(){
	printf("\
|  \\  \\ | / /   |\n\
|      \\_/      |\n\
|      | |      |\n\
|    _||_||_    |\n\
|    |||||||    |\n") ;		
}

void prDOG(){
	printf("\
|@_____@        |\n\
| |o o|     _   |\n\
| |_^_|____//   |\n\
|   | _____*|   |\n\
|   ||     ||   |\n") ;		
}

void prGetHp(){
	printf("\
|      ___      |\n\
|      |||      |\n\
|     (   )     |\n\
|    ( H P )    |\n\
|   (_______)   |\n") ;	
}

void prGetMp(){
	printf("\
|      __       |\n\
|   ___||___    |\n\
|   |      |    |\n\
|   | M  P |    |\n\
|   |______|    |\n") ;	
}

void prLoricae(){
	printf("\
|               |\n\
|  __-000000-__ |\n\
| 00000000000000|\n\
| 00-00000000-00|\n\
| 00 |000000| 00|\n") ;		
}

equip * getEmtpy(){
	equip * ep ;
	ep = (equip *)malloc(sizeof(equip)) ;
	ep -> type = Emtpy ;
	ep -> canuse = 0 ;
	ep -> hpMax_up = 0 ;
	ep -> mpMax_up = 0 ;
	ep -> strength_up = 0 ;
	ep -> energy_up = 0 ;
	
	return ep ;
}

equip * getTANKSuperArm(){
	equip * ep ;
	ep = (equip *)malloc(sizeof(equip)) ;
	ep -> type = TANKarm ;
	ep -> canuse = 0 ;
	ep -> hpMax_up = 50 ;
	ep -> mpMax_up = 0 ;
	ep -> strength_up = 10 ;
	ep -> energy_up = 50 ;
	
	return ep ;
}

equip * getSWORDMANSuperArm(){
	equip * ep ;
	ep = (equip *)malloc(sizeof(equip)) ;
	ep -> type = SWORDMANarm ;
	ep -> canuse = 0 ;
	ep -> hpMax_up = 20 ;
	ep -> mpMax_up = 20 ;
	ep -> strength_up = 50 ;
	ep -> energy_up = 10 ;
	
	return ep ;
}

equip * getMANSuperArm(){
	equip * ep ;
	ep = (equip *)malloc(sizeof(equip)) ;
	ep -> type = MANarm ;
	ep -> canuse = 0 ;
	ep -> hpMax_up = 10 ;
	ep -> mpMax_up = 10 ;
	ep -> strength_up = 10 ;
	ep -> energy_up = 10 ;
	
	return ep ;
}

equip * getENCHANTERSuperArm(){
	equip * ep ;
	ep = (equip *)malloc(sizeof(equip)) ;
	ep -> type = ENCHANTERarm ;
	ep -> canuse = 0 ;
	ep -> hpMax_up = 5 ;
	ep -> mpMax_up = 50 ;
	ep -> strength_up = 5 ;
	ep -> energy_up = 70 ;
	
	return ep ;
}

equip * getGUN(){
	equip * ep ;
	ep = (equip *)malloc(sizeof(equip)) ;
	ep -> type = GUN ;
	ep -> canuse = 0 ;
	ep -> hpMax_up = -10 ;
	ep -> mpMax_up = -10 ;
	ep -> strength_up = 100 ;
	ep -> energy_up = -10 ;
	
	return ep ;
}

equip * getDOG(){
	equip * ep ;
	ep = (equip *)malloc(sizeof(equip)) ;
	ep -> type = DOG ;
	ep -> canuse = 0 ;
	ep -> hpMax_up = 5 ;
	ep -> mpMax_up = 5 ;
	ep -> strength_up = 5 ;
	ep -> energy_up = 5 ;
	
	return ep ;
}

equip * getGetHp(){
	equip * ep ;
	ep = (equip *)malloc(sizeof(equip)) ;
	ep ->type = GetHp ;
	ep -> canuse = 0 ;
	ep -> hpMax_up = 0 ;
	ep -> mpMax_up = 0 ;
	ep -> strength_up = 0 ;
	ep -> energy_up = 0 ;
	
	return ep ;
}

equip * getGetMp(){
	equip * ep ;
	ep = (equip *)malloc(sizeof(equip)) ;
	ep -> type = GetMp ;
	ep -> canuse = 0 ;
	ep -> hpMax_up = 0 ;
	ep -> mpMax_up = 0 ;
	ep -> strength_up = 0 ;
	ep -> energy_up = 0 ;
	
	return ep ;
}


equip * getLoricae(){
	equip * ep ;
	ep = (equip *)malloc(sizeof(equip)) ;
	ep -> type = LORICAE ;
	ep -> canuse = 0 ;
	ep -> hpMax_up = 100 ;
	ep -> mpMax_up = 50 ;
	ep -> strength_up = 5 ;
	ep -> energy_up = 1 ;
	
	return ep ;
}




