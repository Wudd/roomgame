#ifndef _BAD_SLIME_H
#define _BAD_SLIME_H

#include"bad_all.h"
#include"roomui.h"

#include<stdio.h>
#include<stdlib.h>

extern badboy * slimeInit() ;
extern badboy * slime2Init() ;
extern badboy * slime3Init() ;

#endif
