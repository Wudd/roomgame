#ifndef _ROOMUI_H
#define _ROOMUI_H

#include"main.h"
#include"random.h"
#include<stdio.h>
#include<string.h>
#include"hero.h"
#include"init.h"

void prEmtop() ;
void prEmwall() ;
void prDrtop() ;
void prDrwall() ;

extern void prEmroom() ;
extern void prEmroom_2() ;
extern void prMnyroom() ;
extern void prStoroom() ;
extern void prBmnroom1() ;
extern void prBmnroom2() ;
extern void prBmnroom3() ;
extern void prBmnroom4() ;
extern void prBmnroom4_1() ;
extern void prBmnroom4_2() ;
extern void prBmnroomA() ;
extern void prBmnroomB() ;

extern int prRoom(nroom *room , hero_you *u , char * ch) ;

#endif
