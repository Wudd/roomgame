#include"bad_peiqi.h"

void prBad_peiqi(badboy *pq){
	printf("\
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n") ;
	printf("\
 血量:%d/%d               魔法值:%d/%d \n\
 力量:%d        精神力:%d      等级:%d    \n\
 金币:%d	   佩奇 		       \n\
" , pq->hp , pq->hp_max , pq->mp , pq->mp_max , pq->strength , pq->energy , pq->level , pq->diegold) ;
	printf("\
vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv\n") ;	
}

void prPeiqiHurt(){
	prDrtop() ;	
	printf("\
  嗡嗡嗡~~       \n\
  ______()__()_  \n\
 (0 0)_(@) (@))  \n\
      (       )  \n\
=================\n\
=================\n\
      _(_^___)_  \n\
     //QQQQQQQ|| \n\
=================\n\
=================\n\
    ||QQQQQQQQ|| \n\
     O QQQQQQQ O \n\
=================\n\
=================\n\
       _||  ||_  \n\
                 \n") ;	
}

badboy *peiqiInit(){
	badboy *pq ;
	pq = (badboy *)malloc(sizeof(struct BADMAN)) ;
	
	pq->name = "佩奇猪猪" ;
	pq->badnum = PEIQI ;
	pq->hp = 150 ;
	pq->hp_max = 150 ;
	pq->mp = 10 ;
	pq->mp_max = 10 ;
	
	pq->strength = 20 ;
	pq->energy = 10 ;
	
	pq->diegold = 200 ;
	pq->level = 1 ;
	
	pq->type = HEALTH ;
	pq->face = prBmnroom3 ;
	pq->about = prBad_peiqi ;
	pq->hurt = prPeiqiHurt ;
	
	return pq ;
}


