#include"action.h"


char actionBadMan(hero_you *hy , int badnum){
	badboy *da ;
	if(0 <= badnum && badnum < 25){
		da = daimonInit() ;
	}else if(25 <= badnum && badnum < 50){
		da = snakeInit() ;
	}else if(50 <= badnum && badnum < 75){
		da = peiqiInit() ;
	}else if(badnum == -10){
		da = slime2Init() ;
		da->face() ;
	}else if(badnum == -11){
		da = slime3Init() ;
		da->face() ;
	}else{
		da = slimeInit() ;
	}
	
	char choose ;
	da->about(da) ;
	printf("想要战斗吗？    Y/N\n输入 w(往上走) s(往下走) a(往左走) d(往右走) q(退出) e(存档)\n") ;
	while(1){
		choose = inputAction() ;
		if(choose == 'y' || choose == 'Y' || choose == 'n' || choose == 'N' || choose == 'w' \
				|| choose == 's' || choose == 'a' || choose == 'd' || choose == 'q' || choose == 'e'){
			break ;
		}else{
			printf("输入错误!\n") ;
		}
	}
	switch(choose){
		case 'Y' :
		case 'y' :
			system("CLS") ;
			printf("开始战斗...\n") ;
			char ac ;
			while(1){
				
				da->face() ;
				hy->arm() ;
				da->about(da) ;
				hy->face() ;
				prAboutHero(hy) ;
				printf("输入你的动作 [1]攻击 [2]法术 [3]休息 [4]道具 [5]跑路\n") ;
				while(1){
					ac = inputAction() ;
					if(ac == '1' || ac == '2' || ac == '3' || ac == '4' || ac == '5'){
						break ;
					}else if(ac == 1 || ac == 2 || ac == 3 || ac == 4 || ac == 5){
						break ;
					}else{
						printf("输入错误 !\n") ;
					}
				}
				
				system("CLS") ;
				
				switch(ac){
					case '1' ://----------------------------------------英雄攻击行为 
						system("CLS") ;
						printf("攻击！！！\n") ;
						int attackFace = 0 ;
						sleep(1) ;
						int attack = getRandom() ;
						printf("你的攻击动作点数为:%d\n" , attack) ;
						sleep(1) ;
						if(attack <= 20){
							printf("迟钝！！攻击力降低！\n") ;
							attack = -attack ;
							attackFace = 1 ;
						}else if(attack >= 80){
							printf("暴击！！攻击了%s的痛点！\n" , da->name) ;
							attack = (int)(hy->strength * 0.8) ;
							attackFace = 2 ;
						}else{
							printf("普通的来一下\n") ;
							attack = (int)(attack * 0.1) ;
							attackFace = 3 ;
						}
						sleep(1) ;
						switch(attackFace){
							case 1 :
								printf("\n..........手滑了！！！\n") ;
								da->face() ;
								break ;
							case 2 :
								printf("\nAAAAAaaa~~~~~纳命来！！！\n") ;
								da->hurt() ;	
								break ;
							case 3 :
								printf("\n          碰！！！！\n") ;
								da->hurt() ;
								break ;
						}
						
						hy->arm() ;
						
						sleep(1) ; 
						printf("\n最终你对%s造成了 %d 点伤害！！！！\n" , da->name , hy->strength + attack) ;
						da->hp -= (hy->strength + attack) ;
						
						if(hy->job == SWORDMAN){
							int againAttack = getRandom() ;
							if(againAttack >= 60){
								sleep(1) ;
								system("CLS") ;
								printf("你促发了连斩!!!\n") ;
								sleep(1) ;
								int i ; int j = 1;
								for(i = 0 ; i < 10 ; i ++){
									if(j == 1){
										printf("_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-____---\\") ;
										da->hurt() ;
										j = -1 ;
									}else{
										da->face() ;
										hy->arm() ;
										printf("\n\-_-_-_\n-_-_-_-_-_--\n\n\n\n-___n\n\n/") ;
										j = 1 ;
									}
									system("CLS") ;
								}
								system("CLS") ;
								printf("你的二斩对%s造成了 %d 点伤害!!!!!!\n" , da->name , (int)(hy->strength * 0.6) ) ;
								da->hurt() ;
								hy->arm() ;
							}
						}
						
						sleep(2) ;
						system("CLS") ;
						break ;
					case '2' ://---------------------------------------英雄法术行为 
						printf("法术还没开发出来！不准用\n") ;
						break ;
					case '3' ://---------------------------------------英雄休息行为 
						printf("你站着不动..\n") ;
						int hpGet ;
						if(hy->job == TANK){
							hpGet = (int)(hy->hp_max * 0.03) ;
							hy->hp += hpGet ;
						}else{
							hpGet = 10 ;
							hy->hp += hpGet ;
						}
						if(hy->hp > hy->hp_max){
							hy->hp = hy->hp_max ;
						}
						printf("你休息了一下,恢复了 %d 点生命值\n" , hpGet) ;
						break ;
					case '4' ://---------------------------------------英雄使用道具行为 
						printf("道具还没开发出来！不准用\n") ;
						break ;
					case '5' ://---------------------------------------英雄逃跑行为 
						printf("溜了溜了。。\n") ;
						goto OUTF ;
						break ;
				}
				
				int fightBack = getRandom() ;
				if(da->hp >= da->hp_max/20){//---------------------------------战吼 
					if(fightBack <= 100){
						printf("%s对你普通攻击！！\n" , da->name) ;
						sleep(1) ;
						printf("啪！\n") ;
						sleep(1) ;
						int badmanFight = getRandom() ;
						int hyHert ;
						if(badmanFight >= 80){
							printf("%s狠狠地戳中你的痛点！！\n" , da->name) ;
							hyHert = da->strength + (int)(badmanFight * 0.2) ;
						}else{
							printf("%s啃了你一下!\n" , da->name) ;
							hyHert = da->strength ;//怪物普通反击 
						}
						hy->hp -= hyHert ;
						sleep(1) ;
						printf("\n你受到 %d 点伤害！！\n" , hyHert) ;
						
						if(hy->job == TANK){
							da->hp -= (int)(hy->energy * 0.3) + (int)(fightBack * 0.05);
							printf("%s受到你的肉的反弹伤害 %d 点\n" , da->name , (int)(hy->energy * 0.3)) ; 
						}
					}
					sleep(1) ;
				}
				
				if(da->hp < da->hp_max/20){//-------------------------------------------------------亡语 
					if(da->badnum == SLIME){//大史莱姆换中史莱姆 
						printf("你赢了！！\n") ;
						sleep(1) ;
						printf("获得金币%d！\n" , da->diegold) ;
						sleep(1) ;
						hy->gold += da->diegold ;
						printf("中等史莱姆来帮忙!\n") ;
						sleep(1) ;
						actionBadMan(hy , -10) ;//类似递归 
						goto OUTE;
					}else if(da->badnum == SLIME2){//中史莱姆换小史莱姆 
						printf("你赢了！！\n") ;
						sleep(1) ;
						printf("获得金币%d！\n" , da->diegold) ;
						sleep(1) ;
						hy->gold += da->diegold ;
						printf("小小史莱姆来帮忙！\n") ;
						sleep(1) ;
						actionBadMan(hy , -11) ;//类似递归 
						goto OUTE ;
					}//---------------------------------------------------濒死逃跑 
					
					if(da->hp <= 0){//------------------终结 
						goto WIN ;
					}
					//----------------------------逃跑	
					int runaway = getRandom() ;
					printf("%s尝试逃跑！！\n" , da->name) ;
					sleep(1) ;
					printf("%s逃跑动作点数为%d..\n" , da->name , runaway) ;
					sleep(1) ;
					if(runaway >= 80){
						printf("%s逃跑成功并朝你做了个鬼脸！！\n" , da->name) ;
						sleep(1) ;
						printf("但是留下了一些东西。。") ;
						hy->gold += 5 ; printf("你获得了%d块钱!\n" , 5) ;
						goto OUTF ;
					}else{
						printf("%s逃跑失败....(@_@)\n" , da->name) ;
					}
					sleep(1) ;					
					
				}
				
				if(hy->hp <= 0){//-------------------------------------------英雄失败，生命归一点 
					printf("你输了。。(T_T)\n") ;
					hy->hp = 1 ;
					sleep(1) ;
					goto OUTF ;
				}else if(da->hp <= 0){//-------------------------------------胜利，获得金币 
WIN :					
					printf("你赢了！！\n") ;
					printf("获得金币%d！(^_^)\n" , da->diegold) ;
					hy->gold += da->diegold ;
					sleep(1) ;
					goto OUTF ;
				}
				
			}
			break ;
		case 'N' :
		case 'n' :
			system("CLS") ;
			printf("不打了不打了..\n") ;
		
			break ;
		case 'w' :
		case 's' :
		case 'a' :
		case 'd' :
		case 'q' :
		case 'e' :
			return (choose - 20 );
			
	}
OUTF :	//-------------------打印ui标签 
	da->face() ;
	hy->arm() ;
	hy->face() ;
	prAboutHero(hy) ;
OUTE ://--------------------直接退出标签 
	return 0;
}

void actionStronger(hero_you *hy){
	char choose ;
	prStoroom() ;
	printf("托尼：“又是我。。你终于来了。。”\n") ;
	sleep(1) ;
	printf("托尼：“来吧，我可以让你变强。。”\n") ;
	sleep(1) ;
	printf("你：“那么，代价是什么呢？”\n") ;
	sleep(1) ;
	printf("你想进行强化吗？ Y/N\n") ;
	while(1){
		choose = inputAction() ;
		if(choose == 'y' || choose == 'Y' || choose == 'n' || choose == 'N'){
			break ;
		}else{
			printf("输入错误!\n") ;
		}
	}
	switch(choose){
		case 'Y' :
		case 'y' :
			system("CLS") ;
			consumeMoney(hy) ;
			break ;
		case 'N' :
		case 'n' :
			printf("再见。。\n") ;
			sleep(1) ;
			break ;
	}
	system("CLS") ;
}

void actionGoldRm(hero_you *hy , nroom *rm){
	int mon = 20 ;
	if(rm->arrive == 1){
		printf("这里的金币已经被你拿光啦~\n") ;
	}else{
		printf("你获得了$%d金币！\n" , mon) ;
		hy->gold += mon ;
	}
	
	return ;
}

void actionFirstRm(hero_you *hy){
	printf("这是初始房间\n") ;
	hy->hp = hy->hp_max ;
	hy->mp = hy->mp_max ;
	hy->type = HEALTH ;
	printf("所以你的状态恢复啦！！\n\n") ;
	
}



